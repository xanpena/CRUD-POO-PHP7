<?php
    require_once 'config.php';
    $result=false;

    if(!empty($_POST)){
        $id = $_POST['id'];
        $name = $_POST['name'];
        $email = $_POST['email'];

        $sql = "UPDATE users SET name=:name, email=:email WHERE id=:id";
        $query = $pdo->prepare($sql);
        $result = $query->execute([
            'name' => $name,
            'email' => $email,
            'id' => $id
        ]);
    }else{
        $id = $_GET['id'];
    
        $sql = "SELECT * FROM users WHERE id=:id";
        $query = $pdo->prepare($sql);
        $query->execute([
            'id' => $id
        ]);
        $row = $query->fetch(PDO::FETCH_ASSOC);
        $name = $row['name'];
        $email = $row['email'];
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Update user</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h1>Update user</h1>
            <a href="index.php">Home</a>
            <?php if($result){?>
                <div class="alert alert-success">Success!!</div>
            <?php } ?>
            <form action="update.php" method="post">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="<?php echo $name; ?>">
                <br>
                <label for="email">Email</label>
                <input type="text" name="email" id="email" value="<?php echo $email; ?>">
                <br>
                <input type="hidden" name="id" value="<?php echo $id; ?>"> 
                <input type="submit" value="Update">
            </form>
        </div>
    </body>
</html>