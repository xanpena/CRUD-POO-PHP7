<!DOCTYPE html>
<html>
    <head>
        <title>Conexión PDO BBDD</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h1>Databases</h1>
            <ul>
                <li><a href="list.php">List Users</a></li>
                <li><a href="add.php">Add User</a></li>
                <li><a href="fake-login.php">Fake login</a></li>
            </ul>
        </div>
    </body>
</html>