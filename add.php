<?php
    require_once 'config.php';
    $result = false;

    if(!empty($_POST)){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $sql = "INSERT INTO users (name, email, password) VALUES (:name, :email, :password)";
        $query = $pdo->prepare($sql);
        $result = $query->execute([
            'name' => $name,
            'email' => $email,
            'password' => $password
        ]);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Add User</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h1>Add User</h1>
            <a href="index.php">Home</a>
            <?php if($result){?>
                <div class="alert alert-success">Success!!</div>
            <?php } ?>
            <form action="add.php" method="post">
                <label for="name">Name</label>
                <input type="text" name="name" id="name">
                <br>
                <label for="email">Email</label>
                <input type="text" name="email" id="email">
                <br>
                <label for="password">Password</label>
                <input type="password" name="password" id="password">
                <br>
                <input type="submit" value="Save">
            </form>
        </div>
    </body>
</html>