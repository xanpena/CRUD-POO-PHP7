<?php

    $user = null;
    $query = null; 

    if(!empty($_POST)){
        require_once 'config.php';

        $sql = "SELECT * FROM users WHERE email=:email AND password=:password ";
        $query = $pdo->prepare($query);
        $query->execute([
            'email'=>$_POST['email'],
            'password'=>$_POST['password']
        ]);
        /*$queryResult = $pdo->query($query);
        $user = $queryResult->fetch(PDO::FETCH_ASSOC);*/
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Fake Login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h1>Fake Login</h1>
            <a href="index.php">Home</a>
            
            <form action="fake-login.php" method="post">
                <label for="email">Email</label>
                <input type="text" name="email" id="email">
                <br>
                <label for="password">Password</label>
                <input type="text" name="password" id="password">
                <br>
                <input type="submit" value="Sign in">
            </form>
            <h2>Query</h2>
            <div>
                <?php print_r($query); ?>
            </div>
            <h2>User Data</h2>
            <div>
                <?php print_r($user); ?>
            </div>
        </div>
    </body>
</html>